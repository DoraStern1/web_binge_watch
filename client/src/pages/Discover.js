import React, { useState } from "react";
import NavbarComponent from "../components/NavbarComponent";
import ShowDetails from "../components/ShowDetail";
import { Button, Col, Container, Form, Row } from "react-bootstrap";

function Discover() {
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = (e) => {
    e.preventDefault();
    const query = e.target.elements.searchInput.value.trim();
    setSearchQuery(query);
  };

  return (
    <>
      <NavbarComponent />
      <Container>
        <Form inline onSubmit={handleSearch}>
          <Row className="d-flex justify-content-center mx-4 my-4">
            <Col xs={6} >
              <Form.Control
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                name="searchInput"
              />
            </Col>
            <Col xs="auto">
              <Button type="submit">Search</Button>
            </Col>
          </Row>
        </Form>
      </Container>
      <ShowDetails searchQuery={searchQuery} />
    </>
  );
}

export default Discover;
