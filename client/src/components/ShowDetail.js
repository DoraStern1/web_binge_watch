import React, { useState, useEffect, useCallback } from 'react';
import { Container, Card, Row, Col, Spinner, Image, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

function ShowDetails({ searchQuery }) {
  const [showDataList, setShowDataList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [generatedNumbers, setGeneratedNumbers] = useState(new Set());
  const [searchResults, setSearchResults] = useState([]);
  const token = localStorage.getItem('token'); // Assuming you store the token in localStorage
  const navigate = useNavigate();

  const generateUniqueRandomNumbers = useCallback((count) => {
    const newNumbers = [];
    while (newNumbers.length < count) {
      let randomNumber;
      do {
        randomNumber = Math.floor(Math.random() * 5000) + 1;
      } while (generatedNumbers.has(randomNumber));
      newNumbers.push(randomNumber);
      setGeneratedNumbers((prevNumbers) => new Set(prevNumbers).add(randomNumber));
    }
    return newNumbers;
  }, [generatedNumbers]);

  const fetchShowData = async (showIds) => {
    const promises = showIds.map(async (showId) => {
      const response = await fetch(`https://api.tvmaze.com/shows/${showId}`);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    });

    try {
      setLoading(true);
      const shows = await Promise.all(promises);
      setShowDataList((prevShowDataList) => [...prevShowDataList, ...shows]);
      // Save the fetched shows to local storage
      localStorage.setItem('cachedShows', JSON.stringify(shows));
    } catch (error) {
      console.error('Error fetching data: ', error);
    } finally {
      setLoading(false);
    }
  };

  const handleGenerateNewShows = () => {
    setShowDataList([]); // Clear existing show data
    const randomShowIds = generateUniqueRandomNumbers(10);
    fetchShowData(randomShowIds);
  };

  const addToLibrary = async (showId) => {
    if (!token) {
      alert('You need to be logged in to add shows to your library');
      return;
    }

    try {
      const response = await fetch('http://localhost:3001/add-to-library', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token,
        },
        body: JSON.stringify({ showId }),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const result = await response.json();
      alert(result.message);
    } catch (error) {
      console.error('Error adding show to library: ', error);
    }
  };

  const fetchSearchResults = async (query) => {
    setLoading(true);
    try {
      const response = await fetch(`https://api.tvmaze.com/search/shows?q=${query}`);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const results = await response.json();
      setSearchResults(results.map(result => result.show));
    } catch (error) {
      console.error('Error fetching search results: ', error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const generateAndFetchRandomShows = () => {
      const randomShowIds = generateUniqueRandomNumbers(10);
      fetchShowData(randomShowIds);
    };

    if (searchQuery) {
      fetchSearchResults(searchQuery);
    } else {
      // Check for cached shows in local storage
      const cachedShows = JSON.parse(localStorage.getItem('cachedShows'));
      if (cachedShows && cachedShows.length > 0) {
        setShowDataList(cachedShows);
      } else {
        generateAndFetchRandomShows();
      }
    }
  }, [searchQuery, generateUniqueRandomNumbers]); // Re-fetch when searchQuery changes

  const truncateHTML = (html, n) => {
    const div = document.createElement('div');
    div.innerHTML = html;
    let text = div.textContent || div.innerText || '';
    if (text.length > n) {
      text = text.slice(0, n) + '...';
    }
    div.textContent = text; // Set the truncated text back
    return div.innerHTML;
  };

  const displayedShows = searchResults.length ? searchResults : showDataList;

  return (
    <>
    <div className="d-flex justify-content-center my-3 ">
        <Button onClick={handleGenerateNewShows} disabled={loading}>
          Generate New Shows
        </Button>
      </div>
    <Container  fluid className="p-5 pt-1">
      {displayedShows.map((showData, index) => (
        <Card className="mx-auto mb-3" key={index} >
          <Row noGutters>
            <Col md={2} className="d-flex justify-content-center px-4 py-2">
              <Image src={showData.image?.medium} alt={showData.name} fluid />
            </Col>
            <Col md={10} className="text-justify">
              <Row>
                <Card.Body className="my-4 mx-4 " onClick={() => navigate(`/shows/${showData.id}/seasons`)} style={{ cursor: 'pointer' }}>
                  <Card.Title className="text-center">{showData.name} {showData.premiered && (
                    <>
                      {"( "}
                      {new Date(showData.premiered).getFullYear()}
                    </>
                  )}
                    {showData.ended ? (
                      <>
                        {" - "}
                        {new Date(showData.ended).getFullYear()}
                        {" ) "}
                      </>
                    ) : (
                      " - Ongoing )"
                    )}</Card.Title>
                  <Card.Text dangerouslySetInnerHTML={{ __html: truncateHTML(showData.summary, 300) }} className="text-justify d-flex justify-content-center"></Card.Text>
                </Card.Body>
              </Row>
              <Row>
                <Col className="d-flex justify-content-center mb-0 mt-auto">
                  <Button variant="outline-primary" onClick={() => addToLibrary(showData.id)} className="my-3 mx-3">
                    Add Show to My Library
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Card>
      ))}
      {loading && (
        <div className="d-flex justify-content-center">
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
      )}
      
    </Container>
    </>
  );
}

export default ShowDetails;
