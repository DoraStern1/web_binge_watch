const express = require('express');
const mysql = require('mysql2');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3001;

app.use(bodyParser.json());
app.use(cors({
    origin: 'http://localhost:3000'
}));

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'fullastack'
});

db.connect((err) => {
    if (err) {
        console.error('MySQL connection error:', err); // Log connection error
        throw err;
    }
    console.log('MySQL connected...');
});

const verifyToken = (req, res, next) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }

    jwt.verify(token, 'secret', (err, decoded) => {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }
        req.userId = decoded.id;
        next();
    });
};
// Register route
app.post('/register', (req, res) => {
    const { username, password } = req.body;
    const hashedPassword = bcrypt.hashSync(password, 8);

    const sql = 'INSERT INTO user (username, password) VALUES (?, ?)';
    db.query(sql, [username, hashedPassword], (err, result) => {
        if (err) {
            console.error('Error during registration:', err); // Log the error
            return res.status(500).send(err); // Send back the error message
        }
        res.send({ message: 'User registered successfully' });
    });
});


// Login route
app.post('/login', (req, res) => {
    const { username, password } = req.body;
    const sql = 'SELECT * FROM user WHERE username = ?';
    db.query(sql, [username], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        }
        if (results.length === 0) {
            return res.status(400).send({ message: 'User not found' });
        }
        const user = results[0];
        const passwordIsValid = bcrypt.compareSync(password, user.password);
        if (!passwordIsValid) {
            return res.status(401).send({ auth: false, token: null });
        }
        const token = jwt.sign({ id: user.id }, 'secret', { expiresIn: 86400 }); // 24 hours
        res.status(200).send({ auth: true, token });
    });
});


app.get('/get-watched-episodes', verifyToken, (req, res) => {
    const userId = req.userId;
    const sql = 'SELECT episode_id FROM user_episodes WHERE user_id = ?';
  
    db.query(sql, [userId], (err, results) => {
      if (err) {
        console.error('Error fetching watched episodes:', err);
        return res.status(500).send(err);
      }
  
      const watchedEpisodes = results.reduce((acc, row) => {
        acc[row.episode_id] = true;
        return acc;
      }, {});
  
      res.send(watchedEpisodes);
    });
  });


  
  


  app.post('/toggle-episode', verifyToken, (req, res) => {
    const { episodeIds, checked } = req.body;
    const userId = req.userId;
  
    if (!Array.isArray(episodeIds) || episodeIds.length === 0) {
      return res.status(400).json({ error: 'Invalid episodeIds provided' });
    }
  
    const values = episodeIds.map((episodeId) => [userId, episodeId]);
  
    let sql, params;
    if (checked) {
      sql = 'INSERT INTO user_episodes (user_id, episode_id) VALUES ? ON DUPLICATE KEY UPDATE user_id=user_id';
      params = [values];
    } else {
      sql = 'DELETE FROM user_episodes WHERE user_id = ? AND episode_id IN (?)';
      params = [userId, episodeIds];
    }
  
    db.query(sql, params, (err, result) => {
      if (err) {
        console.error('Error toggling episodes:', err);
        return res.status(500).send(err);
      }
  
      const message = checked ? 'Episodes added to your library successfully' : 'Episodes removed from your library successfully';
      res.send({ message });
    });
  });
  
  

  app.post('/add-to-library', (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }

    jwt.verify(token, 'secret', (err, decoded) => {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }

        const userId = decoded.id;
        const { showId } = req.body;

        const sql = 'INSERT INTO user_library (user_id, show_id, date_added) VALUES (?, ?, CURRENT_TIMESTAMP)';
        db.query(sql, [userId, showId], (err, result) => {
            if (err) {
                console.error('Error adding show to library:', err);
                return res.status(500).send(err);
            }
            res.send({ message: 'Show added to library successfully' });
        });
    });
});

app.post('/remove-from-library', (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }

    jwt.verify(token, 'secret', (err, decoded) => {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }

        const userId = decoded.id;
        const { showId } = req.body;

        const sql = 'DELETE FROM user_library WHERE user_id = ? AND show_id = ?';
        db.query(sql, [userId, showId], (err, result) => {
            if (err) {
                console.error('Error removing show from library:', err);
                return res.status(500).send(err);
            }
            res.send({ message: 'Show removed from library successfully' });
        });
    });
});



app.get('/get-user-library', (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }

    jwt.verify(token, 'secret', (err, decoded) => {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }

        const userId = decoded.id;

        const sql = `
            SELECT show_id 
            FROM user_library 
            WHERE user_id = ?
        `;

        db.query(sql, [userId], (err, results) => {
            if (err) {
                console.error('Error fetching user library:', err);
                return res.status(500).send(err);
            }

            const showIds = results.map(result => result.show_id);

            Promise.all(showIds.map(showId => {
                return fetch(`https://api.tvmaze.com/shows/${showId}`)
                    .then(response => response.json())
                    .catch(err => console.error('Error fetching show data:', err));
            }))
            .then(shows => {
                res.send(shows);
            })
            .catch(err => {
                console.error('Error fetching show data:', err);
                res.status(500).send(err);
            });
        });
    });
});

// Update the existing add-comment endpoint
app.post('/add-comment', verifyToken, (req, res) => {
    const userId = req.userId;
    const { showId, comment } = req.body;
  
    if (!comment || typeof comment !== 'string' || comment.trim().length === 0) {
      return res.status(400).send({ error: 'Invalid comment provided' });
    }
  
    const sql = 'INSERT INTO user_comments (user_id, show_id, comment, date_added) VALUES (?, ?, ?, CURRENT_TIMESTAMP)';
    db.query(sql, [userId, showId, comment], (err, result) => {
      if (err) {
        console.error('Error adding comment:', err);
        return res.status(500).send(err);
      }
  
      // Fetch the added comment to return
      const fetchCommentSql = 'SELECT user_comments.id, user_comments.comment, user_comments.date_added, user.username FROM user_comments JOIN user ON user_comments.user_id = user.id WHERE user_comments.id = ?';
      db.query(fetchCommentSql, [result.insertId], (err, commentResult) => {
        if (err) {
          console.error('Error fetching added comment:', err);
          return res.status(500).send(err);
        }
        res.send(commentResult[0]);
      });
    });
  });
  

app.post('/remove-comment', verifyToken, (req, res) => {
    const userId = req.userId;
    const { commentId } = req.body;

    const sql = 'DELETE FROM user_comments WHERE id = ? AND user_id = ?';
    db.query(sql, [commentId, userId], (err, result) => {
        if (err) {
            console.error('Error removing comment:', err);
            return res.status(500).send(err);
        }
        res.send({ message: 'Comment removed successfully' });
    });
});

// Add this endpoint in your backend server code
app.get('/get-comments', (req, res) => {
    const { showId } = req.query;
    const sql = 'SELECT user_comments.id, user_comments.comment, user_comments.date_added, user.username FROM user_comments JOIN user ON user_comments.user_id = user.id WHERE user_comments.show_id = ?';
    db.query(sql, [showId], (err, results) => {
      if (err) {
        console.error('Error fetching comments:', err);
        return res.status(500).send(err);
      }
      res.send(results);
    });
  });
  



// A protected route example
app.get('/me', (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }
    jwt.verify(token, 'secret', (err, decoded) => {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }
        const sql = 'SELECT * FROM user WHERE id = ?';
        db.query(sql, [decoded.id], (err, results) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send(results[0]);
        });
    });
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
