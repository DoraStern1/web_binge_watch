import React, { useContext } from 'react'
import { Navbar, Container } from 'react-bootstrap'

import { NavLink } from "react-router-dom"
import { AuthContext } from './AuthContext';
 

function NavbarComponent() {
  

  const { isAuthenticated, logout } = useContext(AuthContext); // Access isAuthenticated and logout from context

  console.log('isAuthenticated:', isAuthenticated);

  const handleLogout = () => {
    logout(); 
  };
  return (
    <Navbar expand="lg" className="bg-dark p-4">
  <Container>
    <Navbar.Brand href="#home" className="text-light">BingeWatch</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
    {isAuthenticated && (
          <>
          
           <NavLink to="/home" className="link-secondary link-underline-opacity-0 my-2 mx-2 text-light">Home</NavLink> </>
        )}
      
      
        <NavLink to="/discover" className="link-secondary link-underline-opacity-0 my-2 mx-2 text-light">Discover</NavLink>
        {!isAuthenticated && (
          <NavLink to="/login" className="link-secondary link-underline-opacity-0 my-2 mx-2 text-light">Login</NavLink>
        )}
        {isAuthenticated && (
          <>
          
          
          {/*<NavLink to="/profile" className="link-secondary link-underline-opacity-0 my-2 mx-2 text-light">Profile</NavLink>*/}
            <NavLink to="/" onClick={handleLogout} className="link-secondary link-underline-opacity-0 my-2 mx-2 ml-auto text-light">Logout</NavLink>  </>
        )}
      
    </Navbar.Collapse>
  </Container>
</Navbar>
  )
}

export default NavbarComponent

//rfce+Enter