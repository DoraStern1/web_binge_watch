import React, { useState, useContext } from 'react';
import { Col, Button, Row, Container, Card, Form } from 'react-bootstrap';
import NavbarComponent from '../components/NavbarComponent';
import { NavLink } from "react-router-dom";
import axios from 'axios';
import { AuthContext } from '../components/AuthContext';


//const AuthContext = React.createContext(/* ... */);

function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [token] = useState('');
    const { login } = useContext(AuthContext);


  const handleLogin = () => {
    axios.post('http://localhost:3001/login', { username, password })
      .then(response => {
        const newToken = response.data.token; // Assuming response contains token
        login(newToken); // Call login function from context to update token
        alert('Logged in successfully');
        window.location.href = '/';


      })
      .catch(error => {
        console.error(error);
      });
  };

    const getProfile = () => {
        axios.get('http://localhost:3001/me', {
            headers: { 'x-access-token': token }
        })
        .then(response => {
            alert(JSON.stringify(response.data));
        })
        .catch(error => {
            console.error(error);
        });
    };

    return (
      <>
      <NavbarComponent/>
        <Container >
            <Row className="vh-100 d-flex justify-content-center align-items-center">
                <Col md={10} lg={8} xs={12}>
                    <div className="border border-3 border-primary"></div>
                    <Card className="shadow">
                        <Card.Body>
                            <div className="mb-3 mt-4">
                                <h2 className="fw-bold mb-2 justify-content-center">Login</h2>
                                <Form>
                                    <Row className="mb-3">
                                        <Form.Group as={Col} className="mb-3" controlId="formUsername">
                                            <Form.Label className="text-center">Username</Form.Label>
                                            <Form.Control
                                                type="text"
                                                placeholder="Enter your username"
                                                value={username}
                                                onChange={(e) => setUsername(e.target.value)}
                                            />
                                        </Form.Group>
                                    </Row>
                                    <Row className="mb-3">
                                        <Form.Group as={Col} className="mb-3" controlId="formPassword">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control
                                                type="password"
                                                placeholder="Enter your password"
                                                value={password}
                                                onChange={(e) => setPassword(e.target.value)}
                                            />
                                        </Form.Group>
                                    </Row>
                                    <div className="d-flex justify-content-center">
                                        <Button variant="primary" onClick={handleLogin}>Login</Button>
                                    </div>
                                </Form>
                                <div className="mt-3">
                                    <p className="mb-0 text-center">
                                        Don't have an account?{' '}
                                        <NavLink to="/register"   className="text-primary fw-bold" >Sign Up</NavLink>
                                        
                                    </p>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        {token && (
            <>
                <h2>Profile</h2>
                <button onClick={getProfile}>Get Profile</button>
            </>
        )}
        </>
    );
}

export default Login;
