import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
 // Import the other JavaScript file
import { AuthProvider } from './components/AuthContext';
import Discover from './pages/Discover';
import { useEffect } from 'react';


 function App(props) {
  useEffect(() => {
    document.title = 'BingeWatch';
  }, []);
    return (
      <AuthProvider> {/* Wrap the entire App with AuthProvider */}
        <Discover />
      </AuthProvider>
    );
  }

export default App;
