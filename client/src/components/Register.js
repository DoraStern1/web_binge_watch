import React, { useState } from "react";
import { Container, Card, Row, Col, Form, Button } from "react-bootstrap";
import NavbarComponent from "./NavbarComponent";
import axios from "axios";
import { NavLink } from "react-router-dom";

export default function User() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const register = () => {
    axios.post('http://localhost:3001/register', { username, password })
      .then(response => {
        alert(response.data.message);
      })
      .catch(error => {
        console.error(error);
      });
  };

  return (
    <>
      <NavbarComponent />
      <Container>
        <Row className="vh-100 d-flex justify-content-center align-items-center">
          <Col md={10} lg={8} xs={12}>
            <div className="border border-3 border-primary"></div>
            <Card className="shadow">
              <Card.Body>
                <div className="mb-3 mt-4">
                  <h2 className="fw-bold mb-2 text-center">Register</h2>
                  <Form>
                    <Row className="mb-3">
                      <Form.Group as={Col} className="mb-3" controlId="formUsername">
                        <Form.Label className="text-center">Username</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter your username"
                          value={username}
                          onChange={(e) => setUsername(e.target.value)}
                        />
                      </Form.Group>
                    </Row>
                    <Row className="mb-3">
                      <Form.Group as={Col} className="mb-3" controlId="formPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          placeholder="Enter your password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                        />
                      </Form.Group>
                    </Row>
                    
                    <div className="d-flex justify-content-center">
                      <Button variant="primary" onClick={register}>Register</Button>
                    </div>
                  </Form>
                  <div className="mt-3">
                    <p className="mb-0 text-center">
                      Already have an account?{' '}
                      <NavLink to="/login" className="text-primary fw-bold">Login</NavLink>
                    </p>
                  </div>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
