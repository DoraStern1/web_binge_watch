import React, { useState, useEffect } from "react";
import NavbarComponent from "../components/NavbarComponent";
import { Button, Col, Container, Dropdown, Form, Row } from "react-bootstrap";
import UserShowDetail from "../components/UserShowDetail";

function Home() {
  const [searchQuery, setSearchQuery] = useState("");
  const [userLibrary, setUserLibrary] = useState([]);
  const [filteredLibrary, setFilteredLibrary] = useState([]);
  const token = localStorage.getItem("token");

  useEffect(() => {
    const fetchUserLibrary = async () => {
      try {
        const response = await fetch("http://localhost:3001/get-user-library", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": token,
          },
        });

        if (!response.ok) {
          throw new Error("Network response was not ok");
        }

        const results = await response.json();
        setUserLibrary(results);
        setFilteredLibrary(results);
      } catch (error) {
        console.error("Error fetching user library: ", error);
      }
    };

    if (token) {
      fetchUserLibrary();
    }
  }, [token]);

  const handleSearch = (e) => {
    e.preventDefault();
    const query = e.target.elements.searchInput.value.trim().toLowerCase();
    setSearchQuery(query);
    if (query) {
      const filtered = userLibrary.filter(
        (show) =>
          show.name.toLowerCase().includes(query) ||
          show.summary.toLowerCase().includes(query)
      );
      setFilteredLibrary(filtered);
    } else {
      setFilteredLibrary(userLibrary);
    }
  };

  const handleDropdownSelect = (eventKey) => {
    let sortedLibrary = [...filteredLibrary];

    switch (eventKey) {
      case "ascending":
        sortedLibrary.sort((a, b) => a.name.localeCompare(b.name));
        break;
      case "descending":
        sortedLibrary.sort((a, b) => b.name.localeCompare(a.name));
        break;
      default:
        break;
    }

    setFilteredLibrary(sortedLibrary);
  };

  return (
    <>
      <NavbarComponent />
      <Container>
        <Form inline onSubmit={handleSearch}>
          <Row className="d-flex justify-content-center mx-4 my-4">
    
            
            <Col xs={6}>
              <Form.Control
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                name="searchInput"
              />
            </Col>
            <Col xs="auto">
              <Button type="submit">Search</Button>
            </Col>
          </Row>
          <Row  className="d-flex justify-content-center mx-4 my-4">

          <Col>
            <Dropdown onSelect={handleDropdownSelect}>
              <Dropdown.Toggle  id="dropdown-basic">
                Filter
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item eventKey="ascending">
                  Name: ascending
                </Dropdown.Item>
                <Dropdown.Item eventKey="descending">
                  Name: descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            </Col>

          </Row>
        </Form>
      </Container>
      <UserShowDetail 
        showDataList={filteredLibrary} 
        setFilteredLibrary={setFilteredLibrary} // Pass setFilteredLibrary to UserShowDetail
      />
    </>
  );
}

export default Home;
