import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  createBrowserRouter,
  RouterProvider
} from "react-router-dom";
import Login from './pages/Login';
import Register from './components/Register';
import AdminDashboard from './pages/AdminDashboard';
import {  AuthProvider } from './components/AuthContext'
import Discover from './pages/Discover';
import ShowSeasons from './components/ShowSeasons';
import Home from './pages/Home';
import {disableReactDevTools} from'@fvilers/disable-react-devtools';


if (process.env.NODE_ENV=== 'production') disableReactDevTools();


const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "login",
    element: <Login />,
  },
  {
    path: "register",
    element: <Register />,
  },
  {
    path: "admin",
    element: <AdminDashboard />,
  },
  {
    path: "discover",
    element: <Discover/>,
  },
  {
    path: "shows/:showId/seasons",
    element: <ShowSeasons />,
  },
  {
    path: "home",
    element: <Home />,
  }

  
]);

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <AuthProvider>
      <RouterProvider router={router} />
    </AuthProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
