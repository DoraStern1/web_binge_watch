import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

function Comments({ showId }) {
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState("");

  const fetchComments = async () => {
    try {
      const response = await fetch(`http://localhost:3001/get-comments?showId=${showId}`);
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const data = await response.json();
      setComments(data);
    } catch (error) {
      console.error("Error fetching comments: ", error);
    }
  };

  useEffect(() => {
    fetchComments();
  }, [showId]);

  const handleAddComment = async (e) => {
    e.preventDefault();
    const token = localStorage.getItem("token");
    if (!token) {
      alert("You need to be logged in to add comments");
      return;
    }

    try {
      const response = await fetch("http://localhost:3001/add-comment", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": token,
        },
        body: JSON.stringify({
          showId,
          comment: newComment,
        }),
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

    
      const addedComment = await response.json();
      setComments((prev) => [...prev, addedComment]);
      setNewComment("");
    } catch (error) {
      console.error("Error adding comment: ", error);
    }
  };

  const handleDeleteComment = async (commentId) => {
    const token = localStorage.getItem("token");
    if (!token) {
      alert("You need to be logged in to delete comments");
      return;
    }

    try {
      const response = await fetch(`http://localhost:3001/remove-comment`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": token,
        },
        body: JSON.stringify({
          commentId,
        }),
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      // Remove the comment from the state
      setComments((prev) => prev.filter((comment) => comment.id !== commentId));
    } catch (error) {
      console.error("Error deleting comment: ", error);
    }
  };


  return (
    <Container className="pt-3">
      
      <Row className="mt-2">
  <Col>
    <h4 className="text-dark my-4">Comments</h4>
    {comments.length > 0 ? (
      comments.map((comment) => (
        <Card key={comment.id} className="mb-3 "
        style={{  borderRadius: '10px'}}
        >
            <Card.Title className="text-light bg-secondary d-flex text-left px-3 py-2 justify-content-between align-items-center" >{comment.username}
            <Card.Subtitle className="text-light">
              {new Date(comment.date_added).toLocaleString()}
            </Card.Subtitle>


            </Card.Title>
          <Card.Body fluid className='d-flex justify-content-between align-items-center text-wrap'>

            
            <Card.Text className="d-flex align-items-center justify-content-center  text-break" >{comment.comment}</Card.Text>
            
            
            
           
           <Button
                    variant="danger"
                    className="mx-3"
                    onClick={() => handleDeleteComment(comment.id)}
                  >
                    Delete
                  </Button>
           
            
            
          </Card.Body>
        </Card>
      ))
    ) : (
      <p className='text-secondary'>No comments yet. Be the first to comment!</p>
    )}
  </Col>
</Row>

      <Row className="my-5">
        <Col>
        
          <Form onSubmit={handleAddComment}>
            
            <Form.Group controlId="comment">
              <Form.Label className="text-secondary">Add a Comment</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter your comment"
                value={newComment}
                onChange={(e) => setNewComment(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" type="submit" className="mt-2">
              Add new comment.
            </Button>
          </Form>
        </Col>
      </Row>

    </Container>
  );
}

export default Comments;
