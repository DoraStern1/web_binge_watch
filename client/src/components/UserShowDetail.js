import React, { useEffect, useState } from 'react';
import { Container, Card, Row, Col, Image, Button, ProgressBar } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

function UserShowDetail({ showDataList = [], setFilteredLibrary }) {
  const token = localStorage.getItem('token');
  const navigate = useNavigate();
  const [watchedEpisodes, setWatchedEpisodes] = useState({});
  const [showEpisodes, setShowEpisodes] = useState({});

  useEffect(() => {
    const fetchWatchedEpisodes = async () => {
      if (!token) {
        return;
      }
      try {
        const response = await fetch('http://localhost:3001/get-watched-episodes', {
          headers: {
            'x-access-token': token,
          },
        });
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        setWatchedEpisodes(data);
      } catch (error) {
        console.error('Error fetching watched episodes:', error);
      }
    };

    fetchWatchedEpisodes();
  }, [token]);

  useEffect(() => {
    const fetchAllEpisodesForShow = async (showId) => {
      try {
        const response = await fetch(`https://api.tvmaze.com/shows/${showId}/episodes`);
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        setShowEpisodes((prev) => ({ ...prev, [showId]: data }));
      } catch (error) {
        console.error(`Error fetching episodes for show ${showId}:`, error);
      }
    };

    showDataList.forEach(show => {
      fetchAllEpisodesForShow(show.id);
    });
  }, [showDataList]);

  const truncateHTML = (html, n) => {
    const div = document.createElement('div');
    div.innerHTML = html;
    let text = div.textContent || div.innerText || '';
    if (text.length > n) {
      text = text.slice(0, n) + '...';
    }
    div.textContent = text;
    return div.innerHTML;
  };

  const removeFromLibrary = async (showId) => {
    if (!token) {
      alert('You need to be logged in to add shows to your library');
      return;
    }

    try {
      const response = await fetch('http://localhost:3001/remove-from-library', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token,
        },
        body: JSON.stringify({ showId }),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      // Update the filteredLibrary state in Home component
      setFilteredLibrary(prev => prev.filter(show => show.id !== showId));

      const result = await response.json();
      alert(result.message);
    } catch (error) {
      console.error('Error removing show from library:', error);
    }
  };

  const calculateProgress = (show) => {
    const totalEpisodes = showEpisodes[show.id]?.length || 0;
    const watchedEpisodesCount = showEpisodes[show.id]?.filter(episode => watchedEpisodes[episode.id]).length || 0;
    return totalEpisodes ? (watchedEpisodesCount / totalEpisodes) * 100 : 0;
  };

  return (
    <Container fluid className="p-5 pt-1">
      {showDataList.map((showData, index) => (
        <Card className="mx-auto mb-3" key={index}>
          <Row noGutters>
            <Col md={2} className="d-flex justify-content-center px-4 py-2">
              <Image src={showData.image?.medium} alt={showData.name} fluid />
            </Col>
            <Col md={10}>
              <Row>
                <Card.Body className="my-4 mx-4" onClick={() => navigate(`/shows/${showData.id}/seasons`)} style={{ cursor: 'pointer' }}>
                  <Card.Title className="text-center">
                    {showData.name} {showData.premiered && (
                      <>
                        {"( "}
                        {new Date(showData.premiered).getFullYear()}
                      </>
                    )}
                    {showData.ended ? (
                      <>
                        {" - "}
                        {new Date(showData.ended).getFullYear()}
                        {" ) "}
                      </>
                    ) : (
                      " - Ongoing )"
                    )}
                  </Card.Title>
                  <Card.Text dangerouslySetInnerHTML={{ __html: truncateHTML(showData.summary, 300) }}></Card.Text>
                </Card.Body>
              </Row>
              
              <Row>
                <Col className="d-flex justify-content-center mb-0 mt-auto">
                  <Button variant="outline-primary" onClick={() => removeFromLibrary(showData.id)} className="my-3 mx-3">
                    Remove Show From My Library
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col className="px-4">
                  <ProgressBar now={calculateProgress(showData)} label={`${calculateProgress(showData).toFixed(2)}%`} />
                </Col>
              </Row>
            </Col>
            
          </Row>
        </Card>
      ))}
    </Container>
  );
}

export default UserShowDetail;
