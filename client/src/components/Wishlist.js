import React from 'react'
import NavbarComponent from './NavbarComponent'
import { Container } from 'react-bootstrap'

function Wishlist() {
  return (<>
    <NavbarComponent/>
    <Container>
    <div>Wishlist</div>
    </Container>
    </>
  )
}

export default Wishlist