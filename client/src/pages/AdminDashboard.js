import { Card, Container } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import NavbarComponent from "../components/NavbarComponent";

export default function User() {

  const [users, setUsers] = useState([]); // State variable to track users


  // Function to fetch all users from the server
  const fetchUserData = () => {
    fetch("http://localhost:3001/users", {
      headers: {
        'x-access-token': localStorage.getItem('token') // Assuming token is stored in localStorage
      }
    })
    .then(res => res.json())
    .then(result => {
      setUsers(result);
    })
    .catch(error => {
      console.error("Error fetching user data:", error);
    });
  };



  useEffect(() => {
    fetchUserData(); // Fetch users on component mount
  }, []);

  return (
    <>
      <NavbarComponent />
      <Container>
        <h1>Users</h1>
      </Container>
      {users.map(user => (
        <Container key={user.id}>
          <Card>
            <Card.Body>
              Id: {user.id}<br/>
              Username: {user.username}<br/>
             
            </Card.Body>
          </Card>
        </Container>
      ))}
    </>
  );
}
