import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  Row,
  Col,
  Spinner,
  Button,
  Accordion,
  Image,
  Form,
  Tabs,
  Tab,
} from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import NavbarComponent from "./NavbarComponent";
import Comments from "./Comments";


function ShowSeasons() {
  const { showId } = useParams();
  const [seasons, setSeasons] = useState([]);
  const [loading, setLoading] = useState(true);
  const [showDetails, setShowDetails] = useState({});
  const [watchedEpisodes, setWatchedEpisodes] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    const fetchShowDetails = async () => {
      try {
        const response = await fetch(`https://api.tvmaze.com/shows/${showId}`);
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        setShowDetails(data);
      } catch (error) {
        console.error("Error fetching show details: ", error);
      }
    };
  
    const fetchSeasons = async () => {
      try {
        const response = await fetch(
          `https://api.tvmaze.com/shows/${showId}/seasons`
        );
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        const seasonsWithEpisodes = await Promise.all(
          data.map(async (season) => {
            const episodes = await fetchEpisodesForSeason(season.id);
            return { ...season, episodes };
          })
        );
        setSeasons(seasonsWithEpisodes);
      } catch (error) {
        console.error("Error fetching seasons: ", error);
      }
    };
  
    const fetchWatchedEpisodes = async () => {
      const token = localStorage.getItem("token");
      if (!token) {
        return {};
      }
      try {
        const response = await fetch(
          "http://localhost:3001/get-watched-episodes",
          {
            headers: {
              "x-access-token": token,
            },
          }
        );
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        return data;
      } catch (error) {
        console.error("Error fetching watched episodes: ", error);
        return {};
      }
    };
  
    const initializeData = async () => {
      setLoading(true);
      try {
        await fetchShowDetails();
        await fetchSeasons();
        const watchedEpisodes = await fetchWatchedEpisodes();
        setWatchedEpisodes(watchedEpisodes);
      } catch (error) {
        console.error("Error initializing data: ", error);
      } finally {
        setLoading(false);
      }
    };
  
    initializeData();
  }, [showId]);
  
  const displayGenres = (genres) => {
    if (genres && genres.length > 0) {
      return genres.join(", ");
    } else {
      return "No genres available";
    }
  };
  
  const fetchEpisodesForSeason = async (seasonId) => {
    try {
      const response = await fetch(
        `https://api.tvmaze.com/seasons/${seasonId}/episodes`
      );
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const data = await response.json();
      return data;
    } catch (error) {
      console.error(`Error fetching episodes for season ${seasonId}: `, error);
      return [];
    }
  };
  
  const handleAccordionClick = async (index, seasonId) => {
    if (seasons[index].episodes) {
      return;
    }
    try {
      setLoading(true);
      const episodes = await fetchEpisodesForSeason(seasonId);
      setSeasons((prevSeasons) => {
        const updatedSeasons = [...prevSeasons];
        updatedSeasons[index].episodes = episodes;
        return updatedSeasons;
      });
    } finally {
      setLoading(false);
    }
  };
  
  const handleCheckboxChange = async (episodeId, checked) => {
    const token = localStorage.getItem("token");
    if (!token) {
      alert("You need to be logged in to add episodes to your collection");
      return;
    }
  
    try {
      const response = await fetch("http://localhost:3001/toggle-episode", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": token,
        },
        body: JSON.stringify({
          episodeIds: [episodeId], // Pass episodeId as an array for consistency with backend
          checked: checked,
        }),
      });
  
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
  
      // Assuming successful response updates frontend state
      setWatchedEpisodes((prev) => ({
        ...prev,
        [episodeId]: checked,
      }));
    } catch (error) {
      console.error("Error toggling episode: ", error);
    }
  };
  
  
  const handleMarkAllChange = async (seasonId, checked) => {
    const token = localStorage.getItem("token");
    if (!token) {
      alert("You need to be logged in to add episodes to your collection");
      return;
    }
  
    try {
      const season = seasons.find((s) => s.id === seasonId);
      if (season && season.episodes) {
        const response = await fetch("http://localhost:3001/toggle-episode", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": token,
          },
          body: JSON.stringify({
            episodeIds: season.episodes.map((episode) => episode.id),
            checked: checked,
          }),
        });
  
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
  
        // Assuming successful response updates frontend state
        setWatchedEpisodes((prev) => {
          const updatedWatchedEpisodes = { ...prev };
          season.episodes.forEach((episode) => {
            updatedWatchedEpisodes[episode.id] = checked;
          });
          return updatedWatchedEpisodes;
        });
  
        alert("Episodes have been added to your library!");
      }
    } catch (error) {
      console.error("Error toggling all episodes: ", error);
    }
  };
  
  
  return (
    <>
      <NavbarComponent />

      <Container fluid className="text-sm-center p-4 bg-light text-white" style={{ minHeight: '100vh' }}>
        <Row >
          
          <Col
            
            className="d-flex align-items-center justify-content-center my-2 text-justify"
          >
            <h1 className="display-2 text-dark">{showDetails.name}</h1>
          </Col>
          
        </Row>
        <Row className="pt-4">
        <Tabs 
            defaultActiveKey="about"
            id="uncontrolled-tab-example"
            className="mb-3 d-flex align-items-center justify-content-center"
          >
            <Tab eventKey="about" title="About" >
              <Row className="mt-3 pt-3">
            <Col md={1}></Col>
                <Col md={3}  className="d-flex align-items-center justify-content-center my-2">
                <Image 
                
            src={showDetails.image?.medium}
            fluid
            style={{ objectFit: 'cover' , borderRadius: '10px'}}
            ></Image>

                </Col>
                <Col md={7}  className="d-flex align-items-center justify-content-center my-2 py-3">

                <Container style={{disply:'flex', textAlign:'left'}}>
                <h4 className="text-dark">  Show info</h4>
                
                <h5 className="text-secondary">IMDb rating: &#9733;{showDetails.rating?.average}</h5>

                <h5 className="text-dark">  
                {showDetails.summary ? (
                  <span
                    dangerouslySetInnerHTML={{ __html: showDetails.summary }}
                  />
                ) : (
                  "No description available"
                )}
              </h5>

              <h5 className="text-left text-secondary">
                Genres: {displayGenres(showDetails.genres)}
                </h5>
              </Container>
                </Col>
                <Col md={1}></Col>
           
              </Row>
            </Tab>
            <Tab eventKey="episodes" title="Episodes">
              <Container fluid className="pt-3">
                {loading ? (
                  <div className="d-flex justify-content-center">
                    <Spinner animation="border" role="status">
                      <span className="sr-only">Loading...</span>
                    </Spinner>
                  </div>
                ) : (
                  <Accordion defaultActiveKey="0" >
                    {seasons.map((season, index) => (
                      <Accordion.Item
                        key={index}
                        eventKey={index.toString()}
                        onClick={() => handleAccordionClick(index, season.id)}
                      >
                        <Accordion.Header className="mx-2 my-2">
                          Season {season.number} (
                          {season.episodes
                            ? season.episodes.filter(
                                (episode) => watchedEpisodes[episode.id]
                              ).length
                            : 0}
                          /{season.episodeOrder})
                        </Accordion.Header>
                        <Accordion.Body  className="">
                          <Button
                            variant="primary"
                            onClick={() => handleMarkAllChange(season.id, true)}
                            className="mb-3"
                          >
                            Mark All as Watched
                          </Button>
                          <Button
                            variant="secondary"
                            onClick={() =>
                              handleMarkAllChange(season.id, false)
                            }
                            className="mb-3 ms-2"
                          >
                            Unmark All
                          </Button>
                          {season.episodes ? (
                            season.episodes.map((episode, episodeIndex) => (
                              <Card className="mx-auto mb-3" key={episodeIndex}>
                                <Row noGutters className="px-4 py-2">
                                  <Col
                                    md={4}
                                    className="d-flex justify-content-center px-4 py-2"
                                  >
                                    <Image
                                      className="placeholder-image"
                                      src={
                                        episode.image?.medium ||
                                        "./images/depositphotos_247872612-stock-illustration-no-image-available-icon.webp"
                                      }
                                      alt={episode.name}
                                      fluid
                                    />
                                  </Col>
                                  <Col md={7}>
                                    <Card.Body className="my-4 mx-2 text-justify">
                                      <Card.Title>
                                        Episode {episode.number}: {episode.name}
                                      </Card.Title>
                                      <Card.Text
                                        dangerouslySetInnerHTML={{
                                          __html: episode.summary,
                                        }}
                                      ></Card.Text>
                                    </Card.Body>
                                  </Col>

                                  <Col
                                    md={1}
                                    className="d-flex align-items-center justify-content-center my-2"
                                  >
                                    <Form>
                                      <Form.Check
                                        type="checkbox"
                                        id={`episode-${episode.id}`}
                                        label="Mark as watched"
                                        checked={!!watchedEpisodes[episode.id]} // Use !! to ensure a boolean value
                                        onChange={(e) =>
                                          handleCheckboxChange(
                                            episode.id,
                                            e.target.checked
                                          )
                                        }
                                      />
                                    </Form>
                                  </Col>
                                </Row>
                              </Card>
                            ))
                          ) : (
                            <div className="d-flex justify-content-center">
                              <Spinner animation="border" role="status">
                                <span className="sr-only">
                                  Loading episodes...
                                </span>
                              </Spinner>
                            </div>
                          )}
                        </Accordion.Body>
                      </Accordion.Item>
                    ))}
                  </Accordion>
                )}
              </Container>
            </Tab>
            <Tab eventKey="comments" title="Comments">
                <Comments showId={showDetails.id}></Comments>
            </Tab >
          </Tabs >
        </Row>
      </Container>
    </>
  );
}

export default ShowSeasons;
